#!/bin/bash
set +e
cat > .env <<EOF
REGISTRY_USER=${REGISTRY_USER} 
REGISTRY_PASSWORD=${REGISTRY_PASSWORD}
REGISTRY_URL=${REGISTRY_URL}
REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}
TAG=${CI_COMMIT_SHA}
EOF
docker network create -d bridge momo_network || true
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_URL
docker pull $REGISTRY_IMAGE/momo-frontend:$TAG
docker stop momo-frontend || true
docker rm momo-frontend || true
set -e
docker run -d --name momo-frontend \
    -p 80:80 \
    --network=momo_network \
    --restart always \
    --pull always \
    $REGISTRY_IMAGE/momo-frontend:$TAG
