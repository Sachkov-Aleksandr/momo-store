#!/bin/bash
set +e
cat > .env <<EOF
VERSION=${VERSION}
NEXUS_REPO_USER=${NEXUS_REPO_USER}
NEXUS_REPO_PASS=${NEXUS_REPO_PASS}
NEXUS_REPO_URL=${NEXUS_REPO_URL}
NEXUS_AUTH=${NEXUS_AUTH}
EOF

set -e
curl -H "Authorization: Basic ${NEXUS_AUTH}" https://${NEXUS_REPO_URL}/momo-store-sachkov-aleksandr-helm-charts/${VERSION}/momo-store-${VERSION}.tgz --output momo-store-${VERSION}.tgz
helm uninstall momo-store || true
sleep 15 && \
helm upgrade --install momo-store momo-store-${VERSION}.tgz
rm momo-store-${VERSION}.tgz || true
rm .env || true
