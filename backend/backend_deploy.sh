#!/bin/bash
set +e
cat > .env <<EOF
REGISTRY_USER=${REGISTRY_USER} 
REGISTRY_PASSWORD=${REGISTRY_PASSWORD}
REGISTRY_URL=${REGISTRY_URL}
REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}
TAG=$CI_COMMIT_SHA
EOF
docker network create -d bridge momo_network || true
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_URL
docker pull $REGISTRY_IMAGE/momo-backend:$TAG
docker stop momo-backend || true
docker rm momo-backend || true
set -e
docker run -d --name momo-backend \
    -p 8081:8081 \
    --network=momo_network \
    --restart always \
    --pull always \
    $REGISTRY_IMAGE/momo-backend:$TAG
